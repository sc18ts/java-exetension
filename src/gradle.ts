import * as vscode from "vscode";
import { clear } from "./utils";
import { Action } from "./commandDispatcher";

export async function gradleDo(command: Action) {
  let terminal = vscode.window.createTerminal({ name: "Gradle" });
  let programArgs = vscode.workspace.getConfiguration("java").get("arguments");
  clear(terminal);
  terminal.show();
  if (process.platform === "win32") {
    terminal.sendText(
      `.\\gradlew ${command} ${(programArgs && command === Action.run) ? `--args=\"${programArgs}\"` : ""}`
    );
  } else {
    terminal.sendText(
      `./gradlew ${command} ${(programArgs && command === Action.run) ? `--args=\"${programArgs}\"` : ""}`
    );
  }
}



export function initGradleProject(projectName: string) {
  let projectType = vscode.workspace.getConfiguration("gradle").get("SelectProjectType");
  let dsl = vscode.workspace.getConfiguration("gradle").get("dsl");
  let testFramework = vscode.workspace.getConfiguration("gradle").get("SelectTestFramework");

  const buildScript: string = `gradle init --dsl ${dsl} --package ${projectName.toLowerCase()} --project-name ${projectName} --test-framework ${testFramework} --type ${projectType}`;
  let cmdForGradle = vscode.window.createTerminal({ name: "Gradle", });
  vscode.window.onDidCloseTerminal(function (term) {
    if (term.processId === cmdForGradle.processId) {
      cmdForGradle.dispose();
    }
  });
  cmdForGradle.show();
  cmdForGradle.sendText(buildScript);
}

// Opens the exercise file
export let openExerciseFile = (filePath: string) => {
  if (vscode.workspace.workspaceFolders) {
    const openPath = vscode.Uri.file(filePath);
    vscode.workspace.openTextDocument(openPath).then(doc => {
      vscode.window.showTextDocument(doc, vscode.ViewColumn.One, false);
      // .then(e => {
      //   e.edit(edit => {
      //     // edit.insert(new vscode.Position(0, 0), "\n"); // this is a simple hack to open the doc fully(no preview mode)
      //   })
      // });
    });
  }
};

