import { sleep } from "./utils";
import * as vscode from "vscode";
import * as fs from "fs";
import { homedir } from "os";
import { spawnSync } from "child_process";

export async function checkGitLabData() {
  if (!vscode.workspace.getConfiguration("java").get("gitlabOnboarding")) {
    return;
  }
  if (
    !spawnSync("git", ["config", "--global", "user.name"], { encoding: "utf8" })
      .stdout ||
    !spawnSync("git", ["config", "--global", "user.email"], {
      encoding: "utf8",
    }).stdout
  ) {
    await promptGitCredentials();
  }
  if (!fs.existsSync(`${homedir()}/.ssh/uol_gitlab_key`)) {
    genKey();
  }
}

export async function promptGitCredentials() {
  let email = await vscode.window
    .showInputBox({
      prompt: "Enter your university email",
      placeHolder: "student@leeds.ac.uk",
    })
    .then(function (input) {
      return input;
    });
  if (!email) {
    return;
  }
  let name = await vscode.window
    .showInputBox({
      prompt: "Enter your name",
      placeHolder: "John Doe",
    })
    .then(function (input) {
      return input;
    });
  if (!name) {
    return;
  }
  let terminal = vscode.window.createTerminal({ name: "Git" });
  terminal.sendText(`git config --global --replace-all user.name \"${name}\"`);
  terminal.sendText(`git config --global --replace-all user.email ${email}`);
  terminal.sendText("exit");
}

export async function genKey() {
  let terminal = vscode.window.createTerminal({ name: "SSH" });
  if (process.platform === "win32") {
    terminal.sendText(`ssh-keygen -t ed25519 -N [string]::Empty -f \"${homedir()}\\.ssh\\uol_gitlab_key\"`);
  } else {
    terminal.sendText(
      `ssh-keygen -t ed25519 -N '' -f \"${homedir()}/.ssh/uol_gitlab_key\" <<< y`
    );
  }
  terminal.sendText("exit");
  // Wait until terminal exits
  while (!terminal.exitStatus) {
    await sleep(500);
  }
  let key = fs.readFileSync(`${homedir()}/.ssh/uol_gitlab_key.pub`, "utf8");
  let output = vscode.window.createOutputChannel("GitLab SSH");
  output.appendLine("Your new SSH public key for GitLab is as follows:");
  output.appendLine(key);
  output.appendLine(
    "Add it to your GitLab account by following these instructions:"
  );
  output.appendLine(
    "https://docs.gitlab.com/ee/ssh/README.html#add-an-ssh-key-to-your-gitlab-account"
  );
  output.show();
}
