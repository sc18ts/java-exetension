// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import { run, build, test, setArguments } from "./commandDispatcher";
import { genKey, checkGitLabData, promptGitCredentials } from "./gitlab";
import path = require('path');
import { codeFormatter } from './documentFormatter';
import { runStyleCheck, runCodeAnalysis } from './pmd';
import { initGradleProject } from './gradle';
const axios = require("axios"); // fetch api
import { getWebviewPanel, getWebviewContent } from './webview';
import { isFileExist, openExerciseFile, createExerciseFile, isProjectCreated, getFilePath, createDirectory, createFile } from './fileHandler';
import { testExercise, gradeExercise } from "./exerciseGrading";

// this method is called when the extension is activated
// the extension is activated the very first time the command is executed
export async function activate(context: vscode.ExtensionContext) {



	// Test the project
	context.subscriptions.push(
		vscode.commands.registerCommand("java.test", () => {
			test();
		})
	);

	// compiling java
	context.subscriptions.push(
		vscode.commands.registerCommand("java.build", () => {
			build();
		})
	);

	// Executing java
	context.subscriptions.push(
		vscode.commands.registerCommand("java.run", () => {
			run();
		})
	);

	// Change executable's arguments
	context.subscriptions.push(
		vscode.commands.registerCommand("java.arguments", () => {
			setArguments();
		})
	);

	// Formats the java documents
	context.subscriptions.push(
		vscode.commands.registerCommand("java.format", () => {
			codeFormatter();
		})
	);

	// Generate a new SSH key for GitLab and display it
	context.subscriptions.push(
		vscode.commands.registerCommand("java.genKey", () => {
			genKey();
		})
	);

	// Modify git name and emails
	context.subscriptions.push(
		vscode.commands.registerCommand("java.gitLabCredentials", () => {
			promptGitCredentials();
		})
	);

	// Prompt for git user data and create GitLab SSH key if needed
	checkGitLabData();

	// Checks to ensure a project folder is open
	if (!isProjectCreated()) {
		vscode.window.showErrorMessage("Workspace is empty. Please create/open a project");
		return;
	}

	// Generats PMD Checkstyle report
	context.subscriptions.push(vscode.commands.registerCommand('java.checkstyle', () => {
		try {
			createDirectory(getFilePath(".pmd-code-analysis"));
			if (process.platform === 'win32') {
				createFile("\\.pmd-code-analysis\\checkstyle_report.html", "");
			} else {
				createFile("/.pmd-code-analysis/checkstyle_report.html", "");
			}

			runStyleCheck(context.extensionUri);
		} catch (error) {
			vscode.window.showErrorMessage(error);
		}

	}));

	// Generats PMD Code Analysis
	context.subscriptions.push(vscode.commands.registerCommand('java.codeanalysis', () => {
		try {
			createDirectory(getFilePath(".pmd-code-analysis"));
			if (process.platform === 'win32') {
				createFile("\\.pmd-code-analysis\\java_best_practices_report.html", "");
			} else {
				createFile("/.pmd-code-analysis/java_best_practices_report.html", "");
			}
			runCodeAnalysis(context.extensionUri);
		} catch (error) {
			vscode.window.showErrorMessage(error);
		}


	}));

	// Creates new gradle project
	context.subscriptions.push(vscode.commands.registerCommand('java.gradleInit', async () => {
		const getProjectName: string = await vscode.window.showInputBox({ placeHolder: "Enter your project name (Default:App)" }) || "App";
		initGradleProject(getProjectName);
		// setTimeout(() => {
		// 	vscode.commands.executeCommand("workbench.action.reloadWindow");
		// }, 2000);

		setTimeout(() => {
			const projectPath = getFilePath("");
			openExerciseFile(path.join(projectPath, `/app/src/main/java/${getProjectName.toLowerCase()}/App.java`));
		}, 3000);

	}));

	// Creates and shows webview panel on activation
	let panel: any;
	let exercises: any;
	let learningData: any = null;
	context.subscriptions.push(
		vscode.commands.registerCommand('java.activate', async () => {

			// ensures that api link is provided
			let learningAPI = vscode.workspace.getConfiguration("java").get("learningAPI") || "";

			if (!learningAPI) {
				vscode.window.showErrorMessage("API is not provided. Please go to the setting and exercise provide API");
				return;
			}

			try {
				learningData = await axios.get(learningAPI);

			} catch (error) {
				console.log(error);
			}

			// if unabale fetch the data, show an error message
			if (learningData === null) {
				vscode.window.showErrorMessage(`Data from ${learningAPI} could not be fetched. The server may be down or the provided link is invlalid!`);
				return;
			}
			// formatting the data for vscode quickpick
			exercises = learningData.data.map((exercise: {
				excerciseNumber: number;
				title: string;
				description: string;
				exerciseContent: { contentProvided: boolean; javaFileName: string; javaContent: string };
				testing: {
					testingData: [
						{
							className: string,
							functions: [
								{
									functionName: string,
									tests: [
										{
											testName: string,
											input: string,
											result: string
										}
									]
								}
							]
						}
					],
					gradingData: [
						{
							className: string,
							functions: [
								{
									functionName: string,
									tests: [
										{
											points: number,
											inputs: [string],
											resultHash: string
										}
									]
								}
							]
						}
					]
				}
			}) => {
				return {
					label: `Exercise${exercise.excerciseNumber}`,
					detail: exercise.title,
					body: exercise.description,
					javaExercise: {
						contentProvided: exercise.exerciseContent.contentProvided,
						javaFileName: exercise.exerciseContent.javaFileName,
						javaContent: exercise.exerciseContent.javaContent
					},
					testing: {
						testingData: exercise.testing.testingData,
						gradingData: exercise.testing.gradingData
					}
				};
			});


			panel = getWebviewPanel('COMP1721');
		}));

	let testingDataArray: any;
	let classFile: string;
	let newDir: string;

	// Prompts excercises when request and displays them
	context.subscriptions.push(
		vscode.commands.registerCommand('java.learning', async function () {
			const exercise: any = await vscode.window.showQuickPick(exercises, {
				matchOnDetail: true,
			});
			if (!exercise) { return; } // if not selected from picker, returns nothing 

			// createDirectory(exercise.detail);
			newDir = createDirectory(getFilePath(exercise.label)) || "";
			if (exercise.javaExercise.contentProvided) { // First Check if java content is provided, if so then create corrosponding files
				classFile = path.join(newDir, exercise.javaExercise.javaFileName);
				if (isFileExist(classFile)) {
					createExerciseFile(classFile, exercise.javaExercise.javaContent);
				}
				openExerciseFile(classFile);
			}
			panel.webview.html = getWebviewContent(context.extensionUri, panel, exercise);

			testingDataArray = exercise.testing;
		})
	);

	context.subscriptions.push(
		vscode.commands.registerCommand('java.runTesting', () => {
			if (!testingDataArray || !classFile) {
				vscode.window.showErrorMessage("No exercise selected");
			} else {
				testExercise(testingDataArray.testingData, classFile);
			}
		})
	);

	context.subscriptions.push(
		vscode.commands.registerCommand('java.runGrading', () => {
			if (!testingDataArray || !classFile) {
				vscode.window.showErrorMessage("No exercise selected");
			} else {
				gradeExercise(testingDataArray.gradingData, classFile);
			}
		})
	);
}

// this method is called when your extension is deactivated
export function deactivate() { }
