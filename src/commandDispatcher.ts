import * as vscode from "vscode";
import { javaDo } from "./vanillaJava";
import { gradleDo } from "./gradle";
import { fileExists } from "./utils";

export enum Action {
  build = "build",
  run = "run",
  test = "test",
}

export let run = async () => {
  if (await fileExists("gradlew")) {
    gradleDo(Action.run);
  } else {
    javaDo(Action.run);
  }
};

export let build = async () => {
  if (await fileExists("gradlew")) {
    gradleDo(Action.build);
  } else {
    javaDo(Action.build);
  }
};

export let test = async () => {
  if (await fileExists("gradlew")) {
    gradleDo(Action.test);
  } else {
    javaDo(Action.test);
  }
};

export let setArguments = async () => {
  let programArgs = await vscode.window
    .showInputBox({
      prompt: "Enter Java executable arguments",
      placeHolder: "arg1 arg2 arg3",
    })
    .then(function (input) {
      return input;
    }) ?? "";
  vscode.workspace.getConfiguration("java").update("arguments", programArgs);
};
