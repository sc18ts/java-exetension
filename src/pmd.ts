import * as vscode from 'vscode';
import { terminal } from './terminal';
import { getFilePath } from './fileHandler';

export function runStyleCheck(extensionUri: vscode.Uri) {
    let editor = vscode.window.activeTextEditor;
    let languageid = editor?.document.languageId;

    if (languageid !== "java") {
        return; // the document doesn't have java extension
    }

    let fileName = editor?.document.fileName || " ";


    const pmdFolderPath = vscode.Uri.joinPath(extensionUri, 'pmd-analysis', 'pmd-bin-6.32.0').path;
    const pmdCodeStyleCheck = vscode.Uri.joinPath(extensionUri, 'pmd-analysis', 'codestyle.xml').path;
    let checkStyleReportPath: string;
    if (process.platform === "win32") {
        checkStyleReportPath = getFilePath("\\.pmd-code-analysis\\checkstyle_report.html");
    } else {
        checkStyleReportPath = getFilePath("/.pmd-code-analysis/checkstyle_report.html");
    }
    if (process.platform === "win32") {
        // terminal.sendText(`${pmdFolderPath}\pmd.bat -no-cache -d ${fileName} -f html -R ${pmdBestPractices} > ${bestPracticesReportPath}`);
        terminal.sendText(`${pmdFolderPath}\\bin\\pmd.bat -no-cache -d ${fileName} -f html -R ${pmdCodeStyleCheck} > ${checkStyleReportPath}`);
        // clearCMD();
        setTimeout(() => {
            viewReportPrompt(checkStyleReportPath);
        }, 1000);
    } else {
        // terminal.sendText(`${pmdFolderPath}/run.sh pmd -no-cache -d ${fileName} -f html -R ${pmdBestPractices} > ${bestPracticesReportPath}`);
        terminal.sendText(`${pmdFolderPath}/bin/run.sh pmd -no-cache -d ${fileName} -f html -R ${pmdCodeStyleCheck} > ${checkStyleReportPath}`);
        // clearCMD();
        setTimeout(() => {
            viewReportPrompt(checkStyleReportPath);
        }, 1000);

    }
}


export function runCodeAnalysis(extensionUri: vscode.Uri) {
    let editor = vscode.window.activeTextEditor;
    let languageid = editor?.document.languageId;

    if (languageid !== "java") {
        return; // the document doesn't have java extension
    }

    let fileName = editor?.document.fileName || " ";

    const pmdFolderPath = vscode.Uri.joinPath(extensionUri, 'pmd-analysis', 'pmd-bin-6.32.0').path;
    const pmdBestPractices = vscode.Uri.joinPath(extensionUri, 'pmd-analysis', 'bestpractices.xml').path;
    let bestPracticesReportPath: string;
    if (process.platform === "win32") {
        bestPracticesReportPath = getFilePath("\\.pmd-code-analysis\\java_best_practices_report.html");
    } else {
        bestPracticesReportPath = getFilePath("/.pmd-code-analysis/java_best_practices_report.html");
    }
    if (process.platform === "win32") {
        terminal.sendText(`${pmdFolderPath}\\bin\\pmd.bat -no-cache -d ${fileName} -f html -R ${pmdBestPractices} > ${bestPracticesReportPath}`);
        // clearCMD();
        setTimeout(() => {
            viewReportPrompt(bestPracticesReportPath);
        }, 1000);
    } else {
        terminal.sendText(`${pmdFolderPath}/bin/run.sh pmd -no-cache -d ${fileName} -f html -R ${pmdBestPractices} > ${bestPracticesReportPath}`);
        // clearCMD();
        setTimeout(() => {
            viewReportPrompt(bestPracticesReportPath);
        }, 1000);

    }
}

// prompt user a option to view the report
export function viewReportPrompt(reportPath: string) {
    let viewReport = 'View Report';
    vscode.window.showInformationMessage('Click to view the report', viewReport)
        .then(selection => {
            if (selection === viewReport) {
                vscode.env.openExternal(vscode.Uri.parse(
                    reportPath));
            }
        });
}
