import * as vscode from "vscode";
import * as path from "path";
import * as fs from "fs";
import { spawnSync } from "child_process";
import { clear, fileExists, sleep } from "./utils";
import { pbkdf2Sync } from "crypto";

export function createTestingCode(testingDataArray: any) {
    let testCode = `import org.junit.Test;
    import static org.junit.Assert.*;
    
    public class ExerciseTest {`;
    for (const testingData of testingDataArray) {
        let className = testingData.className;
        testCode += `${className} instanceOf${className} = new ${className}();`;
        for (const functionToTest of testingData.functions) {
            let functionName = functionToTest.functionName;
            for (const test of functionToTest.tests) {
                testCode += `@Test
                public void ${test.testName}() {
                    assertEquals(${test.result}, instanceOf${className}.${functionName}(${test.input}));
                }`;
            }
        }
    }
    testCode += "}";
    return testCode;
}

export async function testExercise(testingDataArray: any, exercisePath: string) {
    console.log(testingDataArray);
    let testingCode = createTestingCode(testingDataArray);
    console.log(testingCode);
    let directory = path.dirname(exercisePath);
    let testPath = path.join(directory, "ExerciseTest.java");
    fs.writeFileSync(testPath, testingCode);

    let terminal = vscode.window.createTerminal({ name: "Exercise test" });
    clear(terminal);
    terminal.show();
    await getJunit(terminal);
    terminal.sendText(`javac -cp .java_extension_junit.jar ${exercisePath} ${testPath}`);
    terminal.sendText(`java -jar .java_extension_junit.jar --class-path ${directory} --scan-class-path`);
}

export function createGradingCode(gradingDataArray: any) {
    let tests = [];
    let gradeCode = `public class GradeExercise {`;
    for (const gradingData of gradingDataArray) {
        let className = gradingData.className;
        gradeCode += `${className} instanceOf${className} = new ${className}();`;
        for (const functionToTest of gradingData.functions) {
            let functionName = functionToTest.functionName;
            for (const test of functionToTest.tests) {
                gradeCode += `public void func${tests.length}${test.resultHash}() {`;
                for (const input of test.inputs) {
                    gradeCode += `System.out.print(instanceOf${className}.${functionName}(${input}));`;
                }
                gradeCode += "}";
                tests.push({ testNumber: tests.length, resultHash: test.resultHash, points: test.points, class: className, function: functionName });
            }
        }
    }
    gradeCode += `public static void main(String[] args) {
        GradeExercise grader = new GradeExercise();
        switch (args[0]) {`;
    for (const test of tests) {
        gradeCode += `case "${test.testNumber}${test.resultHash}":
        grader.func${test.testNumber}${test.resultHash}();
        break;`;
    }
    gradeCode += "}}}";
    return { gradeCode: gradeCode, tests: tests };
}

export async function gradeExercise(gradingDataArray: any, exercisePath: string) {
    console.log(gradingDataArray);
    let gradingCode = createGradingCode(gradingDataArray);
    console.log(gradingCode);
    let directory = path.dirname(exercisePath);
    let testPath = path.join(directory, "GradeExercise.java");
    fs.writeFileSync(testPath, gradingCode.gradeCode);
    let terminal = vscode.window.createTerminal({ name: "Exercise test" });
    await getJunit(terminal);
    terminal.sendText(`javac ${exercisePath} ${testPath}`);
    terminal.sendText("exit");
    // Wait until terminal exits
    while (!terminal.exitStatus) {
        await sleep(500);
    }
    testPath = path.join(directory, "GradeExercise");
    let salt = new Uint8Array([203, 4, 209, 122, 252, 83, 44, 195, 130, 61, 157, 231, 176, 45, 36, 93, 178, 110, 198, 144, 156, 58, 167, 26, 201, 228, 120, 96, 235, 174, 232, 245]);
    let points = 0;
    let maxPoints = 0;
    let outputChannel = vscode.window.createOutputChannel("Exercise grading");
    outputChannel.show();
    for (const test of gradingCode.tests) {
        let process = spawnSync("java", ["GradeExercise", `${test.testNumber}${test.resultHash}`], { encoding: "utf8", cwd: directory });
        let outputHash = pbkdf2Sync(process.stdout, salt, 100000, 32, 'sha256').toString('hex');
        maxPoints += test.points;
        if (outputHash === test.resultHash) {
            outputChannel.appendLine(`+${test.points} points for a succesful test of ${test.function} function of ${test.class}`);
            points += test.points;
        } else {
            outputChannel.appendLine(`+0 points for a failed test of ${test.function} function of ${test.class}, hash was ${outputHash} instead of ${test.resultHash}`);
        }
    }
    outputChannel.appendLine(`${points} out of ${maxPoints} points earned`);
}

async function getJunit(terminal: vscode.Terminal) {
    if (!(await fileExists(".java_extension_junit.jar"))) {
        let junitURL =
            "https://repo1.maven.org/maven2/org/junit/platform/junit-platform-console-standalone/1.7.1/junit-platform-console-standalone-1.7.1-all.jar";
        if (process.platform === "win32") {
            terminal.sendText(`Invoke-WebRequest -Uri \"${junitURL}\" -OutFile .java_extension_junit.jar`);
        } else {
            terminal.sendText(`curl -o .java_extension_junit.jar \"${junitURL}\"`);
        }
    }
}