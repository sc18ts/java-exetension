import * as vscode from 'vscode';

export let terminal = vscode.window.createTerminal({ name: "Java", });
vscode.window.onDidCloseTerminal(function (term) {
    if (term.processId === terminal.processId) {
        terminal.dispose();
    }
});
// export let config = vscode.workspace.getConfiguration("java");
