import * as vscode from "vscode";
import { clear, fileExists } from "./utils";
import { Action } from "./commandDispatcher";
import * as path from "path";

export async function javaDo(command: Action) {
  let editor = vscode.window.activeTextEditor;
  if (!editor || editor.document.languageId !== "java") {
    vscode.window.showErrorMessage("Active editor does not contain a Java file");
    return;
  }
  let filePath = editor.document.uri.fsPath;
  let programArgs = vscode.workspace.getConfiguration("java").get("arguments");

  let terminal = vscode.window.createTerminal({ name: "Java" });
  clear(terminal);
  terminal.show();
  switch (command) {
    case Action.run:
      terminal.sendText(`javac ${filePath}`);
      let pathObject = path.parse(filePath);
      terminal.sendText(`java -cp \"${pathObject.dir}\" ${pathObject.name} ${programArgs ?? ""}`);
      break;
    case Action.build:
      terminal.sendText(`javac ${filePath}`);
      break;
    case Action.test:
      await getJunit(terminal);
      terminal.sendText(`javac -cp .java_extension_junit.jar ${filePath}`);
      terminal.sendText(`java -jar .java_extension_junit.jar --class-path . --scan-class-path`);
      break;
  }
}

async function getJunit(terminal: vscode.Terminal) {
  if (!(await fileExists(".java_extension_junit.jar"))) {
    let junitURL =
      "https://repo1.maven.org/maven2/org/junit/platform/junit-platform-console-standalone/1.7.1/junit-platform-console-standalone-1.7.1-all.jar";
    if (process.platform === "win32") {
      terminal.sendText(`Invoke-WebRequest -Uri \"${junitURL}\" -OutFile .java_extension_junit.jar`);
    } else {
      terminal.sendText(`curl -o .java_extension_junit.jar \"${junitURL}\"`);
    }
  }
}