
// This script will be run within the webview itself
// It cannot access the main VS Code APIs directly.
( async function () {
    const vscode = acquireVsCodeApi();

    const doc = document.getElementById("app");

    let exercises = await getData();
    let html = '';

    exercises.forEach(ex => {
      let htmlSegment = `<div class="exercise">
                          <h3>${ex.title}</h3>
                          <p>${ex.description}</p>
                          </div>`;
      html += htmlSegment;
                          
    });

    doc.innerHTML = html;
}());


async function getData() {
  let url = 'https://gist.githubusercontent.com/silent-coding/1a14de6383288ffe9a5c400bbf4db74a/raw/ef75199f40a9bea7048f7af8447ede332baaf8e9/LearningData.json';
  try {
      let res = await fetch(url);
      return await res.json();
  } catch (error) {
      console.log(error);
  }
}
